# netbsd-installer

v0: files only, without boot (netbsd)
    no active partition

v1: bootable, active partition,
    35M, with etc.tar.gz, amd64, kern, and boot (netbsd).
    amd64/netbsd-INSTALL.gz
    i386/netbsd-INSTALL.gz
    Ready to boot and to install. 
    boot.cfg added.

How-To 
------

1.) At boot, press 1: Netbsd ('1' key char)

2.) Select [8] 

3.) Type: 

boot amd64/netbsd-INSTALL.gz 

or

boot i386/netbsd-INSTALL.gz 


Once done, you will have: sysinstall (running on ramdisk!).
You may remove the memstick, all is running in ram.

Have Fun!




